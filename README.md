# All Pilots Have Potential

This mod is designed to encourage retaining your pilots and developing their skills, rather than constantly replacing them. Even the most inexperienced rookie you recruit at the beginning of the game has the potential to develop into an elite, veteran mechwarrior over time.

* Randomly generated pilots that appear for hire at Industrial Hubs will typically have a skill cap between 50 and 58. Less than 1% of pilots will have a skill cap of 59 and only 1 in 1,000 pilots will have a skill cap of 60.
* Increases the skill cap for Freeman, Shenzeng and Fenstrom to 54, 56 and 60 respectively.
* Increases the skill cap for the three starting pilots in Career mode to 53, 55 and 57 for each faction.
* Increases the skill cap for Kestrel Lancers starting pilots in Career mode.
* Starting skill levels for pilots remain unchanged, except for Shenzen and Fenstrom who received small increases.

## Using this mod

To use this mod, install it from [Nexus Mods](https://www.nexusmods.com/mechwarrior5mercenaries/mods/383)

## Using these files in your own mod

Feel free to use these files in your own Mechwarrior 5 mods! Please give credit to the author `moddingisthegame`.

1. Purchase [Mechwarrior 5 for PC](https://www.epicgames.com/store/en-US/p/mechwarrior-5)
2. Install the [Mechwarrior 5 Modding Toolkt](https://www.epicgames.com/store/en-US/p/mechwarrior-5--mod-editor)
3. Install [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
4. Navigate to the Mechwarrior 5 Editor Plugins directory (typically C:\Program Files\Epic Games\MechWarrior5Editor\MW5Mercs\Plugins) in your terminal of choice
5. `git clone https://gitlab.com/mechwarrior5mods/pilotpotential.git`
6. Launch the Mechwarrior 5 Modding Toolkit.
7. Click the dropdown next to Manage Mod and select `pilotpotential`
8. Mod away!

## Requirements

* Heroes of the Inner Sphere DLC
* Legend of the Kestrel Lancers DLC

## Compatibility

* This mod overrides the ProgressionFunctionLib, which controls all kinds of stuff related to campaign progression. 
* This mod overrides the pilot profiles for all recruitable pilots, so will conflict with other mods that modify the same.

## Technical notes

* Modifies the RollPilotSkillInRange Market Place function in ProgressionFunctionLib to set a floor of 8 on pilot skill potential.